package main

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestMailboxGet(t *testing.T) {
	in := "accountId:moo ids:123,456 properties:name"
	ap := newArgParser("ids", "properties")
	args, err := ap.parse(in)
	if err != nil {
		t.Fatal(err)
	}
	expected := map[string]interface{}{
		"accountId":  "moo",
		"ids":        []interface{}{"123", "456"},
		"properties": []interface{}{"name"},
	}
	if !cmp.Equal(args, expected) {
		t.Fatalf("Unexpected %#v", args)
	}
}

func TestMailboxQuery(t *testing.T) {
	in := "accountId:moo filter:{operator:AND conditions:{name:in},{role:inbox}} sort:{property:name isAscending:false}"
	ap := newArgParser("conditions", "sort")
	args, err := ap.parse(in)
	if err != nil {
		t.Fatal(err)
	}
	expected := map[string]interface{}{
		"accountId": "moo",
		"filter": map[string]interface{}{
			"operator": "AND",
			"conditions": []interface{}{
				map[string]interface{}{"name": "in"},
				map[string]interface{}{"role": "inbox"},
			},
		},
		"sort": []interface{}{
			map[string]interface{}{
				"property":    "name",
				"isAscending": false,
			},
		},
	}
	if !cmp.Equal(args, expected) {
		t.Fatalf("Unexpected %#v", args)
	}
}

func TestUnknownLists(t *testing.T) {
	in := "accountId:moo filter:{operator:NOT conditions:[{name:in}]} sort:[{property:name isAscending:false}]"
	ap := newArgParser()
	args, err := ap.parse(in)
	if err != nil {
		t.Fatal(err)
	}
	expected := map[string]interface{}{
		"accountId": "moo",
		"filter": map[string]interface{}{
			"operator": "NOT",
			"conditions": []interface{}{
				map[string]interface{}{"name": "in"},
			},
		},
		"sort": []interface{}{
			map[string]interface{}{
				"property":    "name",
				"isAscending": false,
			},
		},
	}
	if !cmp.Equal(args, expected) {
		t.Fatalf("Unexpected %#v", args)
	}
}
