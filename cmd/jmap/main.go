package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/textproto"
	"os"

	"gitlab.com/alienscience/jmap/client"
	"gitlab.com/alienscience/jmap/jmap"
)

const (
	apiURL = "http://localhost:8081/jmap"
)

var (
	verbose = flag.Bool("v", false, "verbose output including request")
	cmdFile = flag.String("f", "", "read methods from a file, '-' reads from stdin")
)

func main() {
	flag.Usage = usage
	if len(os.Args) < 2 {
		usage()
		os.Exit(1)
	}
	flag.Parse()
	var methods []jmap.MethodRequest
	var err error
	if *cmdFile != "" {
		methods, err = readCmdFile()
	} else {
		var m jmap.MethodRequest
		m, err = parseMethod(flag.Args())
		methods = []jmap.MethodRequest{m}
	}
	if err != nil {
		log.Fatal(err)
	}
	req := client.NewRequestBuilder()
	calls := make([]int, len(methods))
	for i, m := range methods {
		calls[i] = req.Call(m)
	}
	request := req.Build()
	var reqJSON []byte
	reqJSON, err = json.Marshal(request)
	if err != nil {
		log.Fatal("cannot marshal request: ", err)
	}
	if *verbose {
		fmt.Println(string(reqJSON))
	}
	res, err := makeRequest(req.Build())
	if err != nil {
		log.Fatal("request error: ", err)
	}
	fmt.Println(string(res))
}

func usage() {
	fmt.Fprintf(flag.CommandLine.Output(), "Usage: %s JMAP_METHOD [arg=value ...]\n", os.Args[0])
	flag.PrintDefaults()
}

func readCmdFile() ([]jmap.MethodRequest, error) {
	var file *os.File
	var err error
	ret := make([]jmap.MethodRequest, 0, 4)
	if *cmdFile == "-" {
		file = os.Stdin
	} else {
		file, err = os.Open(*cmdFile)
	}
	if err != nil {
		return ret, err
	}
	defer func() { _ = file.Close() }()
	br := bufio.NewReader(file)
	rdr := textproto.NewReader(br)
LINES:
	for {
		line, err := rdr.ReadContinuedLine()
		switch {
		case err == io.EOF:
			break LINES
		case err != nil:
			return ret, err
		}
		m, err := parseLine(line)
		if err != nil {
			return ret, err
		}
		ret = append(ret, m)
	}
	return ret, nil
}

// Request makes a request to a JMAP api.
func makeRequest(req jmap.Request) ([]byte, error) {
	reqBody, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	buf := bytes.NewBuffer(reqBody)
	resp, err := http.Post(apiURL, "application/json", buf)
	if err != nil {
		return nil, err
	}
	defer func() { _ = resp.Body.Close() }()
	if resp.StatusCode >= 400 {
		var problem jmap.Problem
		dec := json.NewDecoder(resp.Body)
		err = dec.Decode(&problem)
		if err != nil {
			return nil, err
		}
		return nil, &problem
	}
	return ioutil.ReadAll(resp.Body)
}
