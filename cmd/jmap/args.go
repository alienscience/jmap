package main

import (
	"errors"
	"strings"

	"gitlab.com/alienscience/jmap/jmap"
)

func parseLine(line string) (jmap.MethodRequest, error) {
	cmdArgs := strings.SplitN(line, " ", 2)
	return parseMethod(cmdArgs)
}

func parseMethod(cmdArgs []string) (jmap.MethodRequest, error) {
	if len(cmdArgs) < 1 {
		return jmap.MethodRequest{}, errors.New("not enough arguments for method call")
	}
	method := cmdArgs[0]
	ret := jmap.MethodRequest{Name: method}
	if len(cmdArgs) < 2 {
		return ret, nil
	}
	parser := newArgParser("ids", "properties", "conditions", "sort")
	var err error
	ret.Args, err = parser.parse(cmdArgs[1])
	return ret, err
}
