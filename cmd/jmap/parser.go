package main

import (
	"errors"

	"gitlab.com/alienscience/parser"
)

type argParser struct {
	lists       map[string]bool
	valueParser *parser.ForwardRef
	state       []*parserState
}

type parserState struct {
	value   map[string]interface{}
	key     string
	current []interface{}
	isList  bool
}

func newArgParser(lists ...string) *argParser {
	listNames := make(map[string]bool, len(lists))
	for _, s := range lists {
		listNames[s] = true
	}
	top := newParserState()
	state := make([]*parserState, 1, 4)
	state[0] = top
	return &argParser{
		lists: listNames,
		state: state,
	}
}

func newParserState() *parserState {
	return &parserState{
		value:   make(map[string]interface{}, 4),
		current: make([]interface{}, 0, 4),
	}
}

func (a *argParser) parse(s string) (map[string]interface{}, error) {
	in := parser.InStr(s)
	// Values are recursive (a value can be a dict which contains values)
	// Create a forward reference to a value parser that will be filled in later.
	a.valueParser = parser.NewForwardRef()
	parser := a.dict()
	a.valueParser.Set(a.value(parser))
	res := parser.Parse(in)
	if !res.Success {
		return a.state[0].value, errors.New("parser failure")
	}
	return a.state[0].value, nil
}

func (a *argParser) dict() parser.P {
	return parser.Seq(
		a.keyValues(),
		parser.ZeroOrMore(
			parser.Seq(sp(), a.keyValues()),
		),
	)
}

func (a *argParser) keyValues() parser.P {
	return parser.Seq(
		a.key(),
		parser.Lit(":"),
		a.parseValues(),
	)
}

func (a *argParser) key() parser.P {
	return parser.Action(
		parser.OneOrMore(parser.NotIn(": \n")),
		func(s string) {
			a.state[len(a.state)-1].key = s
		},
	)
}

func (a *argParser) parseValues() parser.P {
	return parser.Action(
		parser.Seq(
			a.valueParser,
			parser.ZeroOrMore(
				parser.Seq(parser.Lit(","), a.valueParser),
			),
		),
		func(s string) {
			a.addValues()
		},
	)
}

func (a *argParser) value(dict parser.P) parser.P {
	return parser.OneOf(
		a.mapValue(dict),
		a.listValue(),
		a.boolValue(),
		a.strValue(),
	)
}

func (a *argParser) mapValue(dict parser.P) parser.P {
	return parser.Seq(
		parser.Action(
			parser.Lit("{"),
			func(s string) {
				a.pushState()
			},
		),
		dict,
		parser.Action(
			parser.Lit("}"),
			func(s string) {
				a.popState()
			},
		),
	)
}

func (a *argParser) listValue() parser.P {
	return parser.Seq(
		parser.Action(
			parser.Lit("["),
			func(s string) {
				state := a.state[len(a.state)-1]
				state.isList = true
			},
		),
		a.valueParser,
		parser.ZeroOrMore(
			parser.Seq(parser.Lit(","), a.valueParser),
		),
		parser.Lit("]"),
	)
}

func (a *argParser) boolValue() parser.P {
	return parser.OneOf(
		parser.Action(
			parser.Lit("true"),
			func(s string) {
				state := a.state[len(a.state)-1]
				state.current = append(state.current, true)
			},
		),
		parser.Action(
			parser.Lit("false"),
			func(s string) {
				state := a.state[len(a.state)-1]
				state.current = append(state.current, false)
			},
		),
	)
}

func (a *argParser) strValue() parser.P {
	return parser.Action(
		parser.OneOrMore(parser.NotIn("{}, \n")),
		func(s string) {
			state := a.state[len(a.state)-1]
			state.current = append(state.current, s)
		},
	)
}

func (a *argParser) addValues() {
	state := a.state[len(a.state)-1]
	switch {
	case len(state.current) == 0:
		return
	case len(state.current) > 1 || a.lists[state.key] || state.isList:
		values := make([]interface{}, len(state.current))
		copy(values, state.current)
		state.value[state.key] = values
	case len(state.current) == 1:
		state.value[state.key] = state.current[0]
	}
	state.current = state.current[:0]
}

func (a *argParser) pushState() {
	st := newParserState()
	a.state = append(a.state, st)
}

func (a *argParser) popState() {
	if len(a.state) < 2 {
		return
	}
	popped := a.state[len(a.state)-1]
	a.state = a.state[:len(a.state)-1]
	last := a.state[len(a.state)-1]
	last.current = append(last.current, popped.value)
}

func sp() parser.P {
	return parser.OneOrMore(parser.IsIn(" \n"))
}
