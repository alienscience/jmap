package main

import (
	"gitlab.com/alienscience/jmap/jmap"
	"gitlab.com/alienscience/jmap/server"
)

type handler struct{}

func newHandler() server.MailHandler {
	return &handler{}
}

func (h *handler) MailboxGet(args jmap.GetRequest) (*jmap.GetResponse, *jmap.MethodErr) {
	mbox := jmap.Mailbox{
		ID:            "123",
		Name:          "Inbox",
		Role:          "inbox",
		SortOrder:     0,
		TotalEmails:   100,
		UnreadEmails:  10,
		TotalThreads:  80,
		UnreadThreads: 10,
		MyRights: &jmap.MailboxRights{
			MayReadItems:   true,
			MayAddItems:    true,
			MayRemoveItems: true,
			MaySetSeen:     true,
			MaySetKeywords: true,
			MayCreateChild: false,
			MayRename:      false,
			MayDelete:      false,
			MaySubmit:      true,
		},
		IsSubscribed: true,
	}
	err := server.ZeroUnused(&mbox, args.Properties)
	if err != nil {
		return nil, jmap.ServerFail
	}
	ret := jmap.GetResponse{
		AccountID: args.AccountID,
		State:     "TODO",
		List:      []interface{}{mbox},
		NotFound:  []jmap.ID{},
	}
	return &ret, nil
}

func (h *handler) MailboxQuery(args jmap.QueryRequest) (*jmap.QueryResponse, *jmap.MethodErr) {
	return nil, jmap.ServerFail
}

func (h *handler) EmailGet(args jmap.EmailGetRequest) (*jmap.GetResponse, *jmap.MethodErr) {
	return nil, jmap.ServerFail
}
