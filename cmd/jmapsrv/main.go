package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/alienscience/jmap/server"
)

var (
	verbose = flag.Bool("v", false, "verbose - log requests")
)

func main() {
	flag.Parse()
	handler := newHandler()
	options := make([]server.OptionFn, 2, 4)
	options[0] = server.WithMailHandler(handler)
	options[1] = server.AllowOrigin("*")
	if *verbose {
		options = append(options, server.WithLogging(logFn))
	}
	api, err := server.NewAPI(options...)
	if err != nil {
		log.Fatal(err)
	}
	http.Handle("/jmap", api)
	log.Fatal(http.ListenAndServe(":8081", nil))
}

func logFn(msg string) {
	fmt.Println(msg)
}
