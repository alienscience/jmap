package jmap

import (
	"encoding/json"
	"errors"
	"reflect"
	"strings"
)

// Unmarshal JSON into the given struct and return unknown fields
// with type map[string]interface{}.
// This function is used to supported statically typed structs that have
// extra fields that depend on use-case.
func unmarshalWithExtra(iface interface{}, data []byte) (map[string]interface{}, error) {
	// Unmarshal all fields
	raw := make(map[string]json.RawMessage, 8)
	err := json.Unmarshal(data, &raw)
	if err != nil {
		return nil, err
	}
	err = setJsonFields(iface, raw)
	if err != nil {
		return nil, err
	}
	// Unmarshal the extra fields
	ret := make(map[string]interface{}, len(raw))
	for k, v := range raw {
		var i interface{}
		err = json.Unmarshal(v, &i)
		if err != nil {
			return nil, err
		}
		ret[k] = i
	}
	return ret, nil
}

// Marshal JSON from the given struct and include extra fields.
// This function is used to supported statically typed structs that have
// extra fields that depend on use-case.
func marshalWithExtra(iface interface{}, extra map[string]interface{}) ([]byte, error) {
	raw, err := mapJsonFields(iface)
	if err != nil {
		return nil, err
	}
	for k, v := range extra {
		raw[k] = v
	}
	return json.Marshal(raw)
}

// Create a map from Json fields in a struct
func mapJsonFields(iface interface{}) (map[string]interface{}, error) {
	typ := reflect.TypeOf(iface)
	if typ.Kind() != reflect.Struct {
		return nil, errors.New("expected struct to map json fields")
	}
	val := reflect.ValueOf(iface)
	ret := make(map[string]interface{}, typ.NumField())
	for i := 0; i < typ.NumField(); i++ {
		fieldTyp := typ.Field(i)
		js, ok := fieldTyp.Tag.Lookup("json")
		if !ok {
			continue
		}
		jsItems := strings.Split(js, ",")
		name := jsItems[0]
		if name == "-" {
			continue
		}
		value := val.Field(i)
		ret[name] = value.Interface()
	}
	return ret, nil
}

// Set the JSON fields on the given pointer to struct. Leaves only the unused
// fields in the given map.
func setJsonFields(iface interface{}, fields map[string]json.RawMessage) error {
	typ := reflect.TypeOf(iface)
	if typ.Kind() != reflect.Ptr {
		return errors.New("can only set Json fields on pointers to structs")
	}
	val := reflect.ValueOf(iface)
	val = reflect.Indirect(val)
	typ = val.Type()
	if typ.Kind() != reflect.Struct {
		return errors.New("can only set Json fields on pointers to structs")
	}
	for i := 0; i < typ.NumField(); i++ {
		fieldTyp := typ.Field(i)
		js, ok := fieldTyp.Tag.Lookup("json")
		if !ok {
			continue
		}
		jsItems := strings.Split(js, ",")
		name := jsItems[0]
		if name == "-" {
			continue
		}
		field := val.Field(i)
		fieldJson, ok := fields[name]
		if !ok {
			continue
		}
		err := json.Unmarshal(fieldJson, field.Addr().Interface())
		if err != nil {
			return err
		}
		delete(fields, name)
	}
	return nil
}
