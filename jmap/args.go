package jmap

import "encoding/json"

// Function used to unmarshal the args of a response
type unmarshalArgsFn func(json.RawMessage) (interface{}, error)

// Function used to unmarshal the results of a Method/get
type unmarshalGetFn func(json.RawMessage) ([]interface{}, error)

// GetResponseRaw is used to unmarshal a GetResponse
type GetResponseRaw struct {
	AccountID ID              `json:"accountId"`
	State     string          `json:"state"`
	RawList   json.RawMessage `json:"list"`
	NotFound  []ID            `json:"notFound"`
}

// Lookup table of method name to unmarshal function for requests
var unmarshalRequestArgs = map[string]unmarshalArgsFn{
	EchoRequest: unmarshalEchoReq,
	"Email/get": unmarshalEmailGetReq,
}

// Lookup table of method name to unmarshal function for responses
var unmarshalResponseArgs = map[string]unmarshalArgsFn{
	EchoRequest:   unmarshalEchoRes,
	"Mailbox/get": unmarshalGetResponse(unmarshalMailboxGet),
	"Email/get":   unmarshalGetResponse(unmarshalEmailGet),
}

// Unmarshal an Echo request
func unmarshalEchoReq(rawArgs json.RawMessage) (interface{}, error) {
	var reqArgs map[string]interface{}
	err := json.Unmarshal(rawArgs, &reqArgs)
	return reqArgs, err
}

// Unmarshal an Echo response
func unmarshalEchoRes(rawArgs json.RawMessage) (interface{}, error) {
	var resArgs map[string]interface{}
	err := json.Unmarshal(rawArgs, &resArgs)
	return resArgs, err
}

// Unmarshal a Get response using the given function to unmarshal the result list
func unmarshalGetResponse(fn unmarshalGetFn) unmarshalArgsFn {
	return func(rawArgs json.RawMessage) (interface{}, error) {
		var resArgs GetResponseRaw
		err := json.Unmarshal(rawArgs, &resArgs)
		if err != nil {
			return nil, err
		}
		list, err := fn(resArgs.RawList)
		if err != nil {
			return nil, err
		}
		return GetResponse{
			AccountID: resArgs.AccountID,
			State:     resArgs.State,
			List:      list,
			NotFound:  resArgs.NotFound,
		}, nil
	}
}
