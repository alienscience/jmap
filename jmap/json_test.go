package jmap

import (
	"encoding/json"
	"reflect"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/r3labs/diff"
)

type jsonTest struct {
	name string
	js   string
	ds   interface{}
}

var jsonTests = []jsonTest{
	{
		name: "Mailbox/get",
		js: `
{
  "using": [ "urn:ietf:params:jmap:core", "urn:ietf:params:jmap:mail" ],
  "methodCalls": [
    [ "Mailbox/get", {
      "accountId": "u33084183",
      "ids": ["1","2"],
      "properties": ["a","b","c"]
    }, "c1" ]
  ]
}`,
		ds: &Request{
			Using: []string{
				"urn:ietf:params:jmap:core",
				"urn:ietf:params:jmap:mail",
			},
			MethodCalls: []MethodRequest{
				{
					Name: "Mailbox/get",
					ID:   "c1",
					Args: GetRequest{
						AccountID:  "u33084183",
						IDs:        []ID{"1", "2"},
						Properties: []string{"a", "b", "c"},
					},
				},
			},
		},
	},
	{
		name: "Mailbox/get Response",
		js: `
{
  "methodResponses": [
    [ "Mailbox/get", {
      "accountId": "u33084183",
      "state": "78540",
      "list": [{
	  "id": "MB23cfa8094c0f41e6",
	  "name": "Inbox",
	  "parentId": null,
	  "role": "inbox",
	  "sortOrder": 10,
	  "totalEmails": 16307,
	  "unreadEmails": 13905,
	  "totalThreads": 5833,
	  "unreadThreads": 5128,
	  "myRights": {
	    "mayAddItems": true,
	    "mayRename": false,
	    "maySubmit": true,
	    "mayDelete": false,
	    "maySetKeywords": true,
	    "mayRemoveItems": true,
	    "mayCreateChild": true,
	    "maySetSeen": true,
	    "mayReadItems": true
	  },
	  "isSubscribed": true
      }],
      "notFound": []
    }, "c1" ]
  ],
  "sessionState": "75128aab4b1b"
}`,
		ds: &Response{
			MethodResponses: []MethodResponse{
				{
					Name: "Mailbox/get",
					ID:   "c1",
					Args: GetResponse{
						AccountID: "u33084183",
						State:     "78540",
						List: []interface{}{Mailbox{
							ID:            "MB23cfa8094c0f41e6",
							Name:          "Inbox",
							ParentID:      "",
							Role:          "inbox",
							SortOrder:     10,
							TotalEmails:   16307,
							UnreadEmails:  13905,
							TotalThreads:  5833,
							UnreadThreads: 5128,
							MyRights: &MailboxRights{
								MayAddItems:    true,
								MayRename:      false,
								MaySubmit:      true,
								MayDelete:      false,
								MaySetKeywords: true,
								MayRemoveItems: true,
								MayCreateChild: true,
								MaySetSeen:     true,
								MayReadItems:   true,
							},
							IsSubscribed: true}},
						NotFound: []ID{},
					},
				},
			},
			SessionState: "75128aab4b1b",
		},
	},
	{
		name: "Email/get",
		js: `
{
  "using": [ "urn:ietf:params:jmap:core", "urn:ietf:params:jmap:mail" ],
  "methodCalls": [
    [ "Email/get", {
      "accountId": "u33084183",
      "ids": ["1","2"],
      "properties": ["threadId","mailboxIds","from","subject","receivedAt","header:List-POST:asURLs","htmlBody","bodyValues"],
      "bodyProperties": ["partId","blobId","size","type"],
      "fetchHTMLBodyValues": true,
      "maxBodyValueBytes": 256
    }, "c1" ]
  ]
}`,
		ds: &Request{
			Using: []string{
				"urn:ietf:params:jmap:core",
				"urn:ietf:params:jmap:mail",
			},
			MethodCalls: []MethodRequest{
				{
					Name: "Email/get",
					ID:   "c1",
					Args: EmailGetRequest{
						AccountID: "u33084183",
						IDs:       []ID{"1", "2"},
						Properties: []string{"threadId", "mailboxIds", "from", "subject",
							"receivedAt", "header:List-POST:asURLs", "htmlBody", "bodyValues"},
						BodyProperties:      []string{"partId", "blobId", "size", "type"},
						FetchHTMLBodyValues: true,
						MaxBodyValueBytes:   256,
					},
				},
			},
		},
	},
	{
		name: "Email/get Response",
		js: `
{
  "methodResponses": [
  [ "Email/get", {
    "accountId": "abc",
    "state": "41234123231",
    "list": [
      {
	"id": "f123u457",
	"threadId": "ef1314a",
	"mailboxIds": { "f123": true },
	"from": [{ "name": "Joe Bloggs", "email": "joe@example.com" }],
	"subject": "Dinner on Thursday?",
	"receivedAt": "2013-10-13T14:12:00Z",
	"header:List-POST:asURLs": [
	  "mailto:partytime@lists.example.com"
	],
	"htmlBody": [{
	  "partId": "1",
	  "blobId": "B841623871",
	  "size": 283331,
	  "type": "text/html"
	}, {
	  "partId": "2",
	  "blobId": "B319437193",
	  "size": 10343,
	  "type": "text/plain"
	}],
	"bodyValues": {
	  "1": {
	    "isEncodingProblem": false,
	    "isTruncated": true,
	    "value": "<html><body><p>Hello ..."
	  },
	  "2": {
	    "isEncodingProblem": false,
	    "isTruncated": false,
	    "value": "-- Sent by your friendly mailing list ..."
	  }
	}
      }
    ],
    "notFound": [ "f123u456" ]
  }, "#1" ]
  ],
  "sessionState": "75128aab4b1b"
}`,
		ds: &Response{
			MethodResponses: []MethodResponse{
				{
					Name: "Email/get",
					ID:   "#1",
					Args: GetResponse{
						AccountID: "abc",
						State:     "41234123231",
						List: []interface{}{Email{
							ID:         "f123u457",
							ThreadID:   "ef1314a",
							MailboxIDs: map[ID]bool{"f123": true},
							From:       []EmailAddress{{Name: "Joe Bloggs", Email: "joe@example.com"}},
							Subject:    "Dinner on Thursday?",
							ReceivedAt: "2013-10-13T14:12:00Z",
							HeaderExtra: map[string]interface{}{
								"header:List-POST:asURLs": []interface{}{"mailto:partytime@lists.example.com"},
							},
							HTMLBody: []EmailBodyPart{{
								PartID: "1",
								BlobID: "B841623871",
								Size:   283331,
								Type:   "text/html",
							}, {
								PartID: "2",
								BlobID: "B319437193",
								Size:   10343,
								Type:   "text/plain",
							}},
							BodyValues: map[string]EmailBodyValue{
								"1": {
									IsEncodingProblem: false,
									IsTruncated:       true,
									Value:             "<html><body><p>Hello ...",
								},
								"2": {
									IsEncodingProblem: false,
									IsTruncated:       false,
									Value:             "-- Sent by your friendly mailing list ...",
								},
							},
						}},
						NotFound: []ID{"f123u456"},
					},
				},
			},
			SessionState: "75128aab4b1b",
		},
	},
	{
		name: "Core/Echo",
		js: `
{
  "using": [ "urn:ietf:params:jmap:core" ],
  "methodCalls": [
    [ "Core/echo", {
      "hello": true,
      "high": 5
    }, "b3ff" ]
  ]
}`,
		ds: &Request{
			Using: []string{"urn:ietf:params:jmap:core"},
			MethodCalls: []MethodRequest{
				{
					Name: "Core/echo",
					ID:   "b3ff",
					Args: map[string]interface{}{
						"hello": true,
						"high":  float64(5),
					},
				},
			},
		},
	},
	{
		name: "Mailbox/query",
		js: `
{
  "using": [ "urn:ietf:params:jmap:core", "urn:ietf:params:jmap:mail" ],
  "methodCalls": [
    [ "Mailbox/query", {
      "accountId": "u33084183",
      "filter": {"hasAnyRole": true},
      "sort": [{"property": "name", "extraField": "remove"}],
      "sortAsTree": true,
      "filterAsTree": true
    }, "c1" ]
  ]
}`,
		ds: &Request{
			Using: []string{
				"urn:ietf:params:jmap:core",
				"urn:ietf:params:jmap:mail",
			},
			MethodCalls: []MethodRequest{
				{
					Name: "Mailbox/query",
					ID:   "c1",
					Args: QueryRequest{
						AccountID: "u33084183",
						Filter:    map[string]interface{}{"hasAnyRole": true},
						Sort: []Comparator{
							{
								Property: "name",
								Extra:    map[string]interface{}{"extraField": "remove"}},
						},
						Extra: map[string]interface{}{"filterAsTree": true, "sortAsTree": true},
					},
				},
			},
		},
	},
	{
		name: "Mailbox/query Response",
		js: `
{
  "methodResponses": [
    [ "Mailbox/query", {
      "accountId": "u33084183",
      "queryState": "78540",
      "canCalculateChanges": true,
      "position": 1,
      "ids": ["abc1","abc2"],
      "limit": 1000
    }, "c1" ]
  ],
  "sessionState": "75128aab4b1b"
}`,
		ds: &Response{
			MethodResponses: []MethodResponse{
				{
					Name: "Mailbox/query",
					ID:   "c1",
					Args: QueryResponse{
						AccountID:           "u33084183",
						QueryState:          "78540",
						CanCalculateChanges: true,
						Position:            1,
						IDs:                 []ID{"abc1", "abc2"},
						Limit:               1000,
					},
				},
			},
			SessionState: "75128aab4b1b",
		},
	},
	{
		name: "Core/echo Response",
		js: `
{
  "methodResponses": [
    [ "Core/echo", {
      "hello": true,
      "high": 5
    }, "b3ff" ]
  ],
  "sessionState": "123"
}`,
		ds: &Response{
			MethodResponses: []MethodResponse{
				{
					Name: "Core/echo",
					ID:   "b3ff",
					Args: map[string]interface{}{
						"hello": true,
						"high":  float64(5),
					},
				},
			},
			SessionState: "123",
		},
	},
}

func TestJson(t *testing.T) {
	for _, testCase := range jsonTests {
		tc := testCase
		t.Run(tc.name, func(t *testing.T) {
			testUnmarshal([]byte(tc.js), tc.ds, t)
			testMarshal(tc, t)
		})
	}
}

func testUnmarshal(js []byte, ds interface{}, t *testing.T) {
	switch ds.(type) {
	case *Request:
		var target Request
		err := json.Unmarshal(js, &target)
		if err != nil {
			t.Fatal(err)
		}
		if !cmp.Equal(&target, ds) {
			d, _ := diff.Diff(&target, ds)
			t.Fatalf("Unexpected: %#v\n%#v", target, d)
		}
	case *Response:
		var target Response
		err := json.Unmarshal(js, &target)
		if err != nil {
			t.Fatal(err)
		}
		if !cmp.Equal(&target, ds) {
			d, _ := diff.Diff(&target, ds)
			t.Fatalf("Unexpected: %#v\n%#v", target, d)
		}
	default:
		t.Fatalf("Unsupported %s", reflect.TypeOf(ds))
	}
}

func testMarshal(testCase jsonTest, t *testing.T) {
	js, err := json.Marshal(testCase.ds)
	if err != nil {
		t.Fatal(err)
	}
	testUnmarshal(js, testCase.ds, t)
}
