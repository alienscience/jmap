package jmap

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
)

const (
	EchoRequest              = "Core/echo"
	ProblemUnknownCapability = "urn:ietf:params:jmap:error:unknownCapability"
	ProblemNotJSON           = "urn:ietf:params:jmap:error:notJSON"
	ProblemNotRequest        = "urn:ietf:params:jmap:error:notRequest"
	ProblemLimit             = "urn:ietf:params:jmap:error:limit"
)

var (
	// ErrUnexpectedJSON reports an unexpected JSON structure
	ErrUnexpectedJSON = errors.New("unexpected JSON format")

	ServerUnavailable           = &MethodErr{"serverUnavailable"}
	ServerFail                  = &MethodErr{"serverFail"}
	ServerPartialFail           = &MethodErr{"serverPartialFail"}
	UnknownMethod               = &MethodErr{"unknownMethod"}
	InvalidArguments            = &MethodErr{"invalidArguments"}
	InvalidResultReference      = &MethodErr{"invalidResultReference"}
	Forbidden                   = &MethodErr{"forbidden"}
	AccountNotFound             = &MethodErr{"accountNotFound"}
	AccountNotSupportedByMethod = &MethodErr{"accountNotSupportedByMethod"}
	AccountReadOnly             = &MethodErr{"accountReadOnly"}
	AnchorNotFound              = &MethodErr{"anchorNotFound"}
	UnsupportedSort             = &MethodErr{"unsupportedSort"}
	UnsupportedFilter           = &MethodErr{"unsupportedFilter"}
)

// ID is a JMAP record Id
type ID string

// Request is a JMAP Request object
type Request struct {
	Using       []string        `json:"using"`
	MethodCalls []MethodRequest `json:"methodCalls"`
	CreateIDs   map[ID]ID       `json:"createids,omitempty"`
}

// Response is a JMAP Response object
type Response struct {
	MethodResponses []MethodResponse `json:"methodResponses"`
	CreateIDs       map[ID]ID        `json:"createids,omitempty"`
	SessionState    string           `json:"sessionState"`
}

// MethodRequest is a request Invocation object for a JMAP method
type MethodRequest struct {
	Name string
	Args interface{}
	ID   string
}

// MethodResponse is a response Invocation object of a JMAP method
type MethodResponse struct {
	Name string
	Args interface{}
	ID   string
}

type MethodErr struct {
	ErrType string `json:"type"`
}

// GetRequest are the arguments to a JMAP '/get' method call
type GetRequest struct {
	AccountID  ID              `json:"accountId"`
	IDs        []ID            `json:"ids,omitempty"`
	ResultRef  ResultReference `json:"#ids,omitempty"`
	Properties []string        `json:"properties"`
}

// ResultReference references the result of a previous method call
type ResultReference struct {
	ResultOf string `json:"resultOf"`
	Name     string `json:"name"`
	Path     string `json:"path"`
}

// GetResponse are the response arguments returned from a JMAP '/get' method call
type GetResponse struct {
	AccountID ID            `json:"accountId"`
	State     string        `json:"state"`
	List      []interface{} `json:"list"`
	NotFound  []ID          `json:"notFound"`
}

// QueryRequest are the arguments to a JMAP '/query' method call
type QueryRequest struct {
	AccountID      ID                     `json:"accountId"`
	Filter         map[string]interface{} `json:"filter"`
	Sort           []Comparator           `json:"sort,omitempty"`
	Position       int                    `json:"position,omitempty"`
	Anchor         ID                     `json:"anchor,omitempty"`
	AnchorOffset   int                    `json:"anchorOffset,omitempty"`
	Limit          uint                   `json:"limit,omitempty"`
	CalculateTotal bool                   `json:"calculateTotal,omitempty"`
	Extra          map[string]interface{} `json:"-"`
}

// Comparator is included in a '/query' request to give the sort order of results.
type Comparator struct {
	Property    string                 `json:"property"`
	IsAscending bool                   `json:"isAscending,omitempty"`
	Collation   string                 `json:"collation,omitempty"`
	Extra       map[string]interface{} `json:"-"`
}

// QueryResponse is the result of JMAP '/query' method call
type QueryResponse struct {
	AccountID           ID     `json:"accountId"`
	QueryState          string `json:"queryState"`
	CanCalculateChanges bool   `json:"canCalculateChanges"`
	Position            uint   `json:"position,omitempty"`
	IDs                 []ID   `json:"ids"`
	Total               uint   `json:"total,omitempty"`
	Limit               uint   `json:"limit,omitempty"`
}

// Problem defines a Problem details object specified in
// RFC-7807. It holds information on a HTTP request failure.
type Problem struct {
	ProblemType string `json:"type,omitempty"`
	Title       string `json:"title,omitempty"`
	Limit       string `json:"limit,omitempty"`
	Status      int    `json:"status,omitempty"`
	Detail      string `json:"detail,omitempty"`
}

// UnmarshalJSON unmarhals a JMAP request Invocation
func (m *MethodRequest) UnmarshalJSON(data []byte) error {
	var raw []json.RawMessage
	err := json.Unmarshal(data, &raw)
	if err != nil {
		return err
	}
	if len(raw) != 3 {
		return ErrUnexpectedJSON
	}
	err = unmarshalNameID(raw, &m.Name, &m.ID)
	if err != nil {
		return err
	}
	return m.unmarshalArgs(raw[1])
}

// MarshalJSON marshals a JMAP request Invocation
func (m *MethodRequest) MarshalJSON() ([]byte, error) {
	raw := make([]json.RawMessage, 3)
	var err error
	raw[0], err = json.Marshal(m.Name)
	if err != nil {
		return nil, err
	}
	raw[1], err = json.Marshal(m.Args)
	if err != nil {
		return nil, err
	}
	raw[2], err = json.Marshal(m.ID)
	if err != nil {
		return nil, err
	}
	return json.Marshal(raw)
}

func (m *MethodRequest) unmarshalArgs(rawArgs json.RawMessage) error {
	var err error
	// Get a function used to unmarshal the request args
	argsFn, ok := unmarshalRequestArgs[m.Name]
	if ok {
		m.Args, err = argsFn(rawArgs)
		return err
	}
	// If no function is found, unmarshal based on the suffix
	switch {
	case strings.HasSuffix(m.Name, "/get"):
		var reqArgs GetRequest
		err = json.Unmarshal(rawArgs, &reqArgs)
		m.Args = reqArgs
	case strings.HasSuffix(m.Name, "/query"):
		var reqArgs QueryRequest
		err = json.Unmarshal(rawArgs, &reqArgs)
		m.Args = reqArgs
	case m.Name == EchoRequest:
		var reqArgs map[string]interface{}
		err = json.Unmarshal(rawArgs, &reqArgs)
		m.Args = reqArgs
	default:
		return fmt.Errorf("JMAP method '%s' does not meet naming convention", m.Name)
	}
	return err
}

// UnmarshalJSON unmarhals a JMAP response Invocation
func (m *MethodResponse) UnmarshalJSON(data []byte) error {
	var raw []json.RawMessage
	err := json.Unmarshal(data, &raw)
	if err != nil {
		return err
	}
	if len(raw) != 3 {
		return ErrUnexpectedJSON
	}
	err = unmarshalNameID(raw, &m.Name, &m.ID)
	if err != nil {
		return err
	}
	return m.unmarshalArgs(raw[1])
}

// MarshalJSON marshals a JMAP response Invocation
func (m *MethodResponse) MarshalJSON() ([]byte, error) {
	raw := make([]json.RawMessage, 3)
	var err error
	raw[0], err = json.Marshal(m.Name)
	if err != nil {
		return nil, err
	}
	raw[1], err = json.Marshal(m.Args)
	if err != nil {
		return nil, err
	}
	raw[2], err = json.Marshal(m.ID)
	if err != nil {
		return nil, err
	}
	return json.Marshal(raw)
}

func (m *MethodResponse) unmarshalArgs(rawArgs json.RawMessage) error {
	var err error
	// Get a function used to unmarshal the result
	argsFn, ok := unmarshalResponseArgs[m.Name]
	if ok {
		m.Args, err = argsFn(rawArgs)
		return err
	}
	// The method name is unknown - use the suffix
	switch {
	case strings.HasSuffix(m.Name, "/get"):
		var resArgs GetResponse
		err = json.Unmarshal(rawArgs, &resArgs)
		if err != nil {
			return err
		}
		m.Args = resArgs
	case strings.HasSuffix(m.Name, "/query"):
		var resArgs QueryResponse
		err = json.Unmarshal(rawArgs, &resArgs)
		if err != nil {
			return err
		}
		m.Args = resArgs
	default:
		return fmt.Errorf("JMAP method '%s' does not meet naming convention", m.Name)
	}
	return err
}

func unmarshalNameID(raw []json.RawMessage, name *string, id *string) error {
	err := json.Unmarshal(raw[0], name)
	if err != nil {
		return err
	}
	err = json.Unmarshal(raw[2], id)
	if err != nil {
		return err
	}
	return nil
}

// UnmarshalJSON unmarhals a JMAP '/query' request
func (r *QueryRequest) UnmarshalJSON(data []byte) error {
	extra, err := unmarshalWithExtra(r, data)
	if err != nil {
		return err
	}
	r.Extra = extra
	return nil
}

// MarshalJSON marshals a JMAP '/query' request
func (r QueryRequest) MarshalJSON() ([]byte, error) {
	return marshalWithExtra(r, r.Extra)
}

// UnmarshalJSON unmarhals a JMAP '/query' comparator
func (c *Comparator) UnmarshalJSON(data []byte) error {
	extra, err := unmarshalWithExtra(c, data)
	if err != nil {
		return err
	}
	c.Extra = extra
	return nil
}

// MarshalJSON marshals a JMAP '/query' comparator
func (c *Comparator) MarshalJSON() ([]byte, error) {
	return marshalWithExtra(*c, c.Extra)
}

// Result gets the result of a method invocation.
func (r *Response) Result(index int) (*MethodResponse, error) {
	if index >= len(r.MethodResponses) {
		return nil, fmt.Errorf("method result %d does not exist", index)
	}
	res := &r.MethodResponses[index]
	if res.Name == "error" {
		if methodErr, ok := res.Args.(MethodErr); ok {
			return res, methodErr
		}
		return res, errors.New("unknown error returned from method")
	}
	return res, nil
}

// Echo returns the result of a 'Core/echo' method invocation
func (r *Response) Echo(index int) (map[string]interface{}, error) {
	res, err := r.Result(index)
	if err != nil {
		return nil, err
	}
	ret, ok := res.Args.(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("%s result accessed as Core/echo result", res.Name)
	}
	return ret, nil
}

// Get returns the result of a '/get' method invocation
func (r *Response) Get(index int) (GetResponse, error) {
	res, err := r.Result(index)
	if err != nil {
		return GetResponse{}, err
	}
	ret, ok := res.Args.(GetResponse)
	if !ok {
		return GetResponse{}, fmt.Errorf("%s result accessed as Get result", res.Name)
	}
	return ret, nil
}

func (m MethodErr) Error() string {
	return m.ErrType
}

func (p Problem) Error() string {
	return fmt.Sprintf("%s %s %s", p.ProblemType, p.Title, p.Detail)
}
