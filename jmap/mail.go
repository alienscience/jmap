package jmap

import "encoding/json"

// EmailGetRequest are the arguments to a JMAP 'Email/get' method call
type EmailGetRequest struct {
	AccountID           ID              `json:"accountId"`
	IDs                 []ID            `json:"ids,omitempty"`
	ResultRef           ResultReference `json:"#ids,omitempty"`
	Properties          []string        `json:"properties"`
	BodyProperties      []string        `json:"bodyProperties,omitempty"`
	FetchTextBodyValues bool            `json:"fetchTextBodyValues,omitempty"`
	FetchHTMLBodyValues bool            `json:"fetchHTMLBodyValues,omitempty"`
	FetchAllBodyValues  bool            `json:"fetchAllBodyValues,omitempty"`
	MaxBodyValueBytes   uint            `json:"maxBodyValueBytes"`
}

// A Mailbox represents a named set of emails.
type Mailbox struct {
	ID            ID             `json:"id,omitempty"`
	Name          string         `json:"name,omitempty"`
	ParentID      ID             `json:"parentId,omitempty"`
	Role          string         `json:"role,omitempty"`
	SortOrder     uint           `json:"sortOrder,omitempty"`
	TotalEmails   uint           `json:"totalEmails,omitempty"`
	UnreadEmails  uint           `json:"unreadEmails,omitempty"`
	TotalThreads  uint           `json:"totalThreads,omitempty"`
	UnreadThreads uint           `json:"unreadThreads,omitempty"`
	MyRights      *MailboxRights `json:"myRights,omitempty"`
	IsSubscribed  bool           `json:"isSubscribed,omitempty"`
}

// MailboxRights is a set of rights a user has in relation to a Mailbox
type MailboxRights struct {
	MayReadItems   bool `json:"mayReadItems"`
	MayAddItems    bool `json:"mayAddItems"`
	MayRemoveItems bool `json:"mayRemoveItems"`
	MaySetSeen     bool `json:"maySetSeen"`
	MaySetKeywords bool `json:"maySetKeywords"`
	MayCreateChild bool `json:"mayCreateChild"`
	MayRename      bool `json:"mayRename"`
	MayDelete      bool `json:"mayDelete"`
	MaySubmit      bool `json:"maySubmit"`
}

// Email represents an email message
type Email struct {
	ID            ID                        `json:"id,omitempty"`
	BlobID        ID                        `json:"blobId,omitempty"`
	ThreadID      ID                        `json:"threadId,omitempty"`
	MailboxIDs    map[ID]bool               `json:"mailboxIds,omitempty"`
	Keywords      map[ID]bool               `json:"keywords,omitempty"`
	Size          uint                      `json:"size,omitempty"`
	ReceivedAt    string                    `json:"receivedAt,omitempty"`
	Headers       []EmailHeader             `json:"headers,omitempty"`
	HeaderExtra   map[string]interface{}    `json:"-"`
	MessageID     []string                  `json:"messageId,omitempty"`
	InReplyTo     []string                  `json:"inReplyTo,omitempty"`
	References    []string                  `json:"references,omitempty"`
	Sender        []EmailAddress            `json:"sender,omitempty"`
	From          []EmailAddress            `json:"from,omitempty"`
	To            []EmailAddress            `json:"to,omitempty"`
	CC            []EmailAddress            `json:"cc,omitempty"`
	BCC           []EmailAddress            `json:"bcc,omitempty"`
	ReplyTo       []EmailAddress            `json:"replyTo,omitempty"`
	Subject       string                    `json:"subject,omitempty"`
	SentAt        string                    `json:"sentAt,omitempty"`
	BodyStructure EmailBodyPart             `json:"bodyStructure,omitempty"`
	BodyValues    map[string]EmailBodyValue `json:"bodyValues,omitempty"`
	TextBody      []EmailBodyPart           `json:"textBody,omitempty"`
	HTMLBody      []EmailBodyPart           `json:"htmlBody,omitempty"`
	Attachments   []EmailBodyPart           `json:"attachments,omitempty"`
	HasAttachment bool                      `json:"hasAttachment,omitempty"`
	Preview       string                    `json:"preview,omitempty"`
}

// EmailBodyPart holds a MIME part of an email message
type EmailBodyPart struct {
	PartID      string          `json:"partId"`
	BlobID      ID              `json:"blobId"`
	Size        uint            `json:"size"`
	Headers     []EmailHeader   `json:"headers"`
	Name        string          `json:"name"`
	Type        string          `json:"type"`
	Charset     string          `json:"charset"`
	Disposition string          `json:"disposition"`
	CID         string          `json:"cid"`
	Language    []string        `json:"language"`
	Location    string          `json:"location"`
	SubParts    []EmailBodyPart `json:"subParts"`
}

// EmailBodyValue holds a text part of an email message
type EmailBodyValue struct {
	Value             string `json:"value"`
	IsEncodingProblem bool   `json:"isEncodingProblem"`
	IsTruncated       bool   `json:"isTruncated"`
}

// EmailAddress represents an email address
type EmailAddress struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

// EmailHeader contains a single email header
type EmailHeader struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// UnmarshalJSON unmarhals a JMAP email
func (e *Email) UnmarshalJSON(data []byte) error {
	extra, err := unmarshalWithExtra(e, data)
	if err != nil {
		return err
	}
	e.HeaderExtra = extra
	return nil
}

// MarshalJSON marshals a JMAP email
func (e Email) MarshalJSON() ([]byte, error) {
	return marshalWithExtra(e, e.HeaderExtra)
}

func unmarshalEmailGetReq(raw json.RawMessage) (interface{}, error) {
	getReq := EmailGetRequest{}
	err := json.Unmarshal(raw, &getReq)
	return getReq, err
}

func unmarshalEmailGet(rawList json.RawMessage) ([]interface{}, error) {
	var emails []Email
	err := json.Unmarshal(rawList, &emails)
	if err != nil {
		return nil, err
	}
	ret := make([]interface{}, len(emails))
	for i, email := range emails {
		ret[i] = email
	}
	return ret, nil
}

func unmarshalMailboxGet(rawList json.RawMessage) ([]interface{}, error) {
	var mboxes []Mailbox
	err := json.Unmarshal(rawList, &mboxes)
	if err != nil {
		return nil, err
	}
	ret := make([]interface{}, len(mboxes))
	for i, mbox := range mboxes {
		ret[i] = mbox
	}
	return ret, nil
}
