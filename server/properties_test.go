package server

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/alienscience/jmap/jmap"
)

func TestZeroUnused(t *testing.T) {
	mbox := jmap.Mailbox{
		ID:        "123",
		Name:      "Name",
		ParentID:  "Parent",
		Role:      "role",
		SortOrder: 1,
	}
	ZeroUnused(&mbox, []string{"name", "role"})
	expected := jmap.Mailbox{
		Name: "Name",
		Role: "role",
	}
	if !cmp.Equal(mbox, expected) {
		t.Fatalf("Unexpected %#v", mbox)
	}
}
