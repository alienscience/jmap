package server

import "gitlab.com/alienscience/jmap/jmap"

type MailHandler interface {
	MailboxGet(args jmap.GetRequest) (*jmap.GetResponse, *jmap.MethodErr)
	MailboxQuery(args jmap.QueryRequest) (*jmap.QueryResponse, *jmap.MethodErr)
	EmailGet(args jmap.EmailGetRequest) (*jmap.GetResponse, *jmap.MethodErr)
}

type mailShim struct {
	inner MailHandler
}

func (s mailShim) Get(name string, args interface{}) (*jmap.GetResponse, *jmap.MethodErr) {
	switch name {
	case "Mailbox/get":
		req, ok := args.(jmap.GetRequest)
		if !ok {
			return nil, jmap.InvalidArguments
		}
		return s.inner.MailboxGet(req)
	case "Email/get":
		req, ok := args.(jmap.EmailGetRequest)
		if !ok {
			return nil, jmap.InvalidArguments
		}
		return s.inner.EmailGet(req)
	default:
		return nil, jmap.UnknownMethod
	}
}

func (s mailShim) Query(name string, args jmap.QueryRequest) (*jmap.QueryResponse, *jmap.MethodErr) {
	switch name {
	case "Mailbox/query":
		return s.inner.MailboxQuery(args)
	default:
		return nil, jmap.UnknownMethod
	}
}
