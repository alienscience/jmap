package server

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/alienscience/jmap/jmap"
)

type TestCase struct {
	name        string
	req         string
	handler     MailHandler
	res         *jmap.Response
	contentType string
	problem     *jmap.Problem
}

type handlerFuncs struct {
	mailboxGet func(jmap.GetRequest) (*jmap.GetResponse, *jmap.MethodErr)
}

var httpTests = []TestCase{
	{
		name: "echo",
		req: `
{
  "using": [ "urn:ietf:params:jmap:core" ],
  "methodCalls": [
    [ "Core/echo", {
      "hello": true,
      "high": 5
    }, "b3ff" ]
  ]
}`,
		res: &jmap.Response{
			MethodResponses: []jmap.MethodResponse{
				{
					Name: "Core/echo",
					ID:   "b3ff",
					Args: map[string]interface{}{
						"hello": true,
						"high":  float64(5),
					},
				},
			},
			SessionState: "TODO",
		},
	},
	{
		name: "Mailbox/get",
		req: `
{
  "using": [ "urn:ietf:params:jmap:core", "urn:ietf:params:jmap:mail" ],
  "methodCalls": [
    [ "Mailbox/get", {
      "accountId": "u33084183",
      "ids": ["MB23cfa8094c0f41e6"],
      "properties": ["name","id","role"]
    }, "c1" ]
  ]
}`,
		res: &jmap.Response{
			MethodResponses: []jmap.MethodResponse{
				{
					Name: "Mailbox/get",
					ID:   "c1",
					Args: jmap.GetResponse{
						AccountID: "u33084183",
						State:     "TODO",
						NotFound:  []jmap.ID{},
						List: []interface{}{
							jmap.Mailbox{
								ID:   "MB23cfa8094c0f41e6",
								Name: "Inbox",
								Role: "inbox",
							},
						},
					},
				},
			},
			SessionState: "TODO",
		},
		handler: handlerFuncs{
			mailboxGet: func(args jmap.GetRequest) (*jmap.GetResponse, *jmap.MethodErr) {
				return &jmap.GetResponse{
					AccountID: args.AccountID,
					State:     "TODO",
					List: []interface{}{
						jmap.Mailbox{
							ID:   args.IDs[0],
							Name: "Inbox",
							Role: "inbox",
						},
					},
				}, nil
			},
		},
	},
	{
		name:        "contentType",
		req:         "Hello world",
		contentType: "text/plain",
		problem: &jmap.Problem{
			ProblemType: jmap.ProblemNotJSON,
			Status:      http.StatusBadRequest,
			Detail:      "JMAP expects application/json.",
		},
	},
}

func TestHTTPHandler(t *testing.T) {
	for _, testCase := range httpTests {
		tc := testCase
		t.Run(tc.name, func(t *testing.T) {
			testHandler(t, tc)
		})
	}
}

func testHandler(t *testing.T, testCase TestCase) {
	api, err := NewAPI(WithMailHandler(testCase.handler))
	if err != nil {
		t.Fatal(err)
	}
	ts := httptest.NewServer(api)
	defer ts.Close()
	contentType := "application/json"
	if len(testCase.contentType) > 0 {
		contentType = testCase.contentType
	}
	resp, err := http.Post(ts.URL, contentType, strings.NewReader(testCase.req))
	if err != nil {
		t.Fatal(err)
	}
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		t.Fatal(err)
	}
	if testCase.problem != nil {
		var res jmap.Problem
		err = json.NewDecoder(bytes.NewBuffer(body)).Decode(&res)
		if err != nil {
			t.Fatal(err)
		}
		if !cmp.Equal(&res, testCase.problem) {
			t.Fatalf("Unexpected:\n %#v\n from:\n%s\n", res, string(body))
		}
	} else {
		var res jmap.Response
		err = json.NewDecoder(bytes.NewBuffer(body)).Decode(&res)
		if err != nil {
			t.Fatal(err)
		}
		if !cmp.Equal(&res, testCase.res) {
			t.Fatalf("Unexpected:\n %#v\n from:\n%s\n", res, string(body))
		}
	}
}

func (h handlerFuncs) MailboxGet(args jmap.GetRequest) (*jmap.GetResponse, *jmap.MethodErr) {
	return h.mailboxGet(args)
}
