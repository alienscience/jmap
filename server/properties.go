package server

import (
	"errors"
	"reflect"
	"strings"
)

// ZeroUnused takes a pointer to a struct, and zeros any fields that do
// not appear in properties. If properties is null or empty then no fields
// will be zeroed.
// This function is useful to return only properties that appear in a
// request by zeroing fields that will not be marshaled into a json
// response.
func ZeroUnused(iface interface{}, properties []string) error {
	if properties == nil || len(properties) == 0 {
		return nil
	}
	typ := reflect.TypeOf(iface)
	if typ.Kind() != reflect.Ptr {
		return errors.New("can only reset properties on pointers to structs")
	}
	val := reflect.ValueOf(iface)
	val = reflect.Indirect(val)
	typ = val.Type()
	if typ.Kind() != reflect.Struct {
		return errors.New("can only reset properties on pointers to structs")
	}
	props := make(map[string]struct{}, len(properties))
	for _, p := range properties {
		props[p] = struct{}{}
	}
	for i := 0; i < typ.NumField(); i++ {
		fieldTyp := typ.Field(i)
		field := val.Field(i)
		js, ok := fieldTyp.Tag.Lookup("json")
		if !ok {
			continue
		}
		jsItems := strings.Split(js, ",")
		name := jsItems[0]
		if _, ok := props[name]; ok {
			continue
		}
		// Zero the field
		zero := reflect.Zero(fieldTyp.Type)
		field.Set(zero)
	}
	return nil
}
