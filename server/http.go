// Package server provides http.Handler types to that implement a JMAP API.
package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/alienscience/jmap/jmap"
)

// A MethodHandler handles JMAP method calls.
type MethodHandler interface {
	Get(name string, args interface{}) (*jmap.GetResponse, *jmap.MethodErr)
	Query(name string, args jmap.QueryRequest) (*jmap.QueryResponse, *jmap.MethodErr)
}

// An API is a http.Handler that implements a JMAP API.
type API struct {
	allowOrigin string
	logFunc     func(string)
	inner       MethodHandler
}

// An OptionFn is used to configure the API.
type OptionFn func(a *API) error

// AllowOrigin sets the Access-Control-Allow-Origin HTTP header for the JMAP api.
func AllowOrigin(origin string) OptionFn {
	return func(a *API) error {
		a.allowOrigin = origin
		return nil
	}
}

// WithMailHandler sets a JMAP Handler that will be called when method calls for
// the JMAP mail API are received.
func WithMailHandler(h MailHandler) OptionFn {
	return func(a *API) error {
		a.inner = mailShim{h}
		return nil
	}
}

// WithMethodHandler sets a low level JMAP Handler that will be called when
// JMAP method calls are received. A MethodHandler should only be needed when using
// the JMAP protocol for something other than email.
func WithMethodHandler(h MethodHandler) OptionFn {
	return func(a *API) error {
		a.inner = h
		return nil
	}
}

// WithLogging sets a function that will be called to log requests.
func WithLogging(logFunc func(string)) OptionFn {
	return func(a *API) error {
		a.logFunc = logFunc
		return nil
	}
}

// NewAPI creates a new JMAP API.
func NewAPI(options ...OptionFn) (*API, error) {
	ret := &API{}
	for _, o := range options {
		err := o(ret)
		if err != nil {
			return nil, err
		}
	}
	return ret, nil
}

// ServeHTTP serves HTTP requests to a JMAP api.
func (a *API) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if a.allowOrigin != "" {
		w.Header().Set("Access-Control-Allow-Origin", a.allowOrigin)
	}
	switch r.Method {
	case http.MethodPost: // OK
	case http.MethodOptions:
		w.Header().Set("Access-Control-Allow-Methods", "POST,OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
		w.WriteHeader(http.StatusNoContent)
		return
	default:
		problem := jmap.Problem{
			Title:  fmt.Sprintf("Unsupported Method: %s", r.Method),
			Detail: "JMAP requests should use HTTP POST",
			Status: http.StatusBadRequest,
		}
		httpError(w, problem)
		return
	}
	if r.Header.Get("Content-Type") != "application/json" {
		problem := jmap.Problem{
			ProblemType: jmap.ProblemNotJSON,
			Detail:      "JMAP expects application/json.",
			Status:      http.StatusBadRequest,
		}
		httpError(w, problem)
		return
	}
	var req jmap.Request
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		problem := jmap.Problem{
			ProblemType: jmap.ProblemNotRequest,
			Detail:      err.Error(),
			Status:      http.StatusBadRequest,
		}
		httpError(w, problem)
		return
	}
	if a.logFunc != nil {
		a.logRequest(req)
	}
	res := a.handleRequest(req)
	// Write the response directly. If marshaling the JSON leads to an error
	// it is too late to change the HTTP Status and return a JSON formatted error.
	_ = json.NewEncoder(w).Encode(res)
}

func (a *API) handleRequest(req jmap.Request) jmap.Response {
	res := make([]jmap.MethodResponse, len(req.MethodCalls))
	for i, method := range req.MethodCalls {
		res[i] = a.handleMethod(method)
	}
	return jmap.Response{
		MethodResponses: res,
		SessionState:    "TODO",
	}
}

func (a *API) handleMethod(call jmap.MethodRequest) jmap.MethodResponse {
	name := call.Name
	if name == jmap.EchoRequest {
		return jmap.MethodResponse(call)
	}
	var responseArgs interface{}
	var err *jmap.MethodErr
	switch {
	case strings.HasSuffix(name, "/get"):
		var resArgs *jmap.GetResponse
		resArgs, err = a.inner.Get(name, call.Args)
		if err != nil {
			break
		}
		// NotFound slice should not be nil
		if resArgs != nil && resArgs.NotFound == nil {
			resArgs.NotFound = []jmap.ID{}
		}
		responseArgs = *resArgs
	case strings.HasSuffix(name, "/query"):
		args, ok := call.Args.(jmap.QueryRequest)
		if !ok {
			err = jmap.InvalidArguments
			break
		}
		var resArgs *jmap.QueryResponse
		resArgs, err = a.inner.Query(name, args)
		if err != nil {
			break
		}
		responseArgs = *resArgs
	default:
		err = jmap.UnknownMethod
	}
	ret := jmap.MethodResponse{ID: call.ID}
	if err != nil {
		ret.Name = "error"
		ret.Args = *err
	} else {
		ret.Name = call.Name
		ret.Args = responseArgs
	}
	return ret
}

func (a *API) logRequest(req jmap.Request) {
	j, err := json.Marshal(req)
	if err == nil {
		a.logFunc(string(j))
	}
}

func httpError(w http.ResponseWriter, problem jmap.Problem) {
	js, err := json.Marshal(problem)
	status := problem.Status
	if err != nil {
		http.Error(w, strconv.Itoa(status), status)
	} else {
		http.Error(w, string(js), status)
	}
}
