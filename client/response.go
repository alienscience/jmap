package client

import (
	"errors"

	"gitlab.com/alienscience/jmap/jmap"
)

var (
	ErrInvalidType = errors.New("invalid type in response")
)

func Mailboxes(r jmap.GetResponse) ([]jmap.Mailbox, error) {
	mboxes := make([]jmap.Mailbox, len(r.List))
	var ok bool
	for i, iface := range r.List {
		mboxes[i], ok = iface.(jmap.Mailbox)
		if !ok {
			return mboxes, ErrInvalidType
		}
	}
	return mboxes, nil
}
