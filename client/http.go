// Package client provides a JMAP client implementation.
package client

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/alienscience/jmap/jmap"
)

// Client makes requests to a JMAP api.
type Client struct {
	url string
	h   http.Client
}

// NewClient creates a new JMAP client.
func NewClient(url string) *Client {
	return &Client{url: url}
}

// Request makes a request to a JMAP api.
func (c *Client) Request(req jmap.Request) (*jmap.Response, error) {
	reqBody, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}
	buf := bytes.NewBuffer(reqBody)
	resp, err := c.h.Post(c.url, "application/json", buf)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode >= 400 {
		var problem jmap.Problem
		dec := json.NewDecoder(resp.Body)
		defer func() { _ = resp.Body.Close() }()
		err = dec.Decode(&problem)
		if err != nil {
			return nil, err
		}
		return nil, &problem
	}
	dec := json.NewDecoder(resp.Body)
	defer func() { _ = resp.Body.Close() }()
	var res jmap.Response
	err = dec.Decode(&res)
	if err != nil {
		return nil, err
	}
	return &res, nil
}
