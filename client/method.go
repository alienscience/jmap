package client

import "gitlab.com/alienscience/jmap/jmap"

// Echo is a method call that requests a JMAP api to echo the given arguments.
func Echo(args map[string]interface{}) jmap.MethodRequest {
	return jmap.MethodRequest{
		Name: "Core/echo",
		Args: args,
	}
}

// MailboxGet gets Mailbox details from a JMAP api.
func MailboxGet(accountID jmap.ID, ids []jmap.ID, properties []string) jmap.MethodRequest {
	return jmap.MethodRequest{
		Name: "Mailbox/get",
		Args: jmap.GetRequest{
			AccountID:  accountID,
			IDs:        ids,
			Properties: properties,
		},
	}
}
