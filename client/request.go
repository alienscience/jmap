package client

import (
	"strconv"

	"gitlab.com/alienscience/jmap/jmap"
)

/*
	req := NewRequestBuilder()
	req.Using(CoreCap, MailCap)
	echo := req.Call(Echo(map[string]interface{}{"hello": true, "high": 5}))
	mbox := req.Call(MailboxGet("u33084183", []jmap.ID{"1", "2"}, []string{"a", "b", "c"}))
	res, err := api.Request(req.Build())
	if err != nil {
		log.Fatal(err)
	}
	echoRes, err := res.Result(echo)
	mboxRes, err := res.Result(mbox)
*/

// RequestBuilder builds a JMAP request.
type RequestBuilder struct {
	req jmap.Request
}

// NewRequestBuilder creates a new RequestBuilder.
func NewRequestBuilder() *RequestBuilder {
	return &RequestBuilder{
		req: jmap.Request{
			Using: []string{
				"urn:ietf:params:jmap:core",
				"urn:ietf:params:jmap:mail",
			},
			MethodCalls: make([]jmap.MethodRequest, 0, 2),
		},
	}
}

// Call adds a method call to a JMAP Request.
// The method call ID in jmap.MethodReq will be overwritten with an internally calculated
// value and does not need to be set.
// Call returns an index to the call added.
func (r *RequestBuilder) Call(call jmap.MethodRequest) int {
	callID := len(r.req.MethodCalls)
	call.ID = strconv.Itoa(callID)
	r.req.MethodCalls = append(r.req.MethodCalls, call)
	return callID
}

// Build creates a JMAP Request.
func (r *RequestBuilder) Build() jmap.Request {
	return r.req
}
