package main

import (
	"fmt"

	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
	"gitlab.com/alienscience/jmap/client"
	"gitlab.com/alienscience/jmap/jmap"
)

const jmapURL = "http://localhost:8081/jmap"

func main() {
	vecty.SetTitle("Hello JMAP!")
	page := PageView{
		echo: Echo{
			echo: make(map[string]interface{}),
		},
	}
	vecty.RenderInto("body", &page)
	jmapRequest(&page)
	vecty.RenderBody(&page)
}

func jmapRequest(view *PageView) error {
	api := client.NewClient(jmapURL)
	req := client.NewRequestBuilder()
	echo := req.Call(client.Echo(map[string]interface{}{
		"hello": true,
		"high":  5,
	}))
	mbox := req.Call(
		client.MailboxGet(
			"account123",
			[]jmap.ID{"123"},
			[]string{"id", "name", "unreadEmails"}),
	)
	res, err := api.Request(req.Build())
	if err != nil {
		return err
	}
	echoRes, err := res.Echo(echo)
	if err != nil {
		return err
	}
	mboxRes, err := res.Get(mbox)
	if err != nil {
		return err
	}
	view.echo.echo = echoRes
	view.mboxes.list, err = client.Mailboxes(mboxRes)
	return err
}

// PageView is our main page component.
type PageView struct {
	vecty.Core
	echo   Echo
	mboxes Mboxes
}

// Render implements the vecty.Component interface.
func (p *PageView) Render() vecty.ComponentOrHTML {
	return elem.Body(
		elem.Heading1(vecty.Text("Hello JMAP")),
		p.echo.Render(),
		p.mboxes.Render(),
	)
}

// Mboxes is a component that shows the results of an Mailbox/get request
type Mboxes struct {
	vecty.Core
	list []jmap.Mailbox
}

func (m *Mboxes) Render() vecty.ComponentOrHTML {
	elems := make([]vecty.MarkupOrChild, len(m.list)+1)
	i := 0
	elems[i] = elem.Heading2(vecty.Text("Mailboxes"))
	i++
	for _, v := range m.list {
		elems[i] = elem.Paragraph(vecty.Text(v.Name))
		i++
	}
	return elem.Div(elems...)
}

// Echo is a component that shows the results of an Core/echo request
type Echo struct {
	vecty.Core
	echo map[string]interface{}
}

func (e *Echo) Render() vecty.ComponentOrHTML {
	elems := make([]vecty.MarkupOrChild, len(e.echo)+1)
	i := 0
	elems[i] = elem.Heading2(vecty.Text("Echo"))
	i++
	for k, v := range e.echo {
		kv := fmt.Sprint(k, " = ", v)
		elems[i] = elem.Paragraph(vecty.Text(kv))
		i++
	}
	return elem.Div(elems...)
}
