module gitlab.com/alienscience/jmap

go 1.13

require (
	github.com/google/go-cmp v0.4.0
	github.com/gopherjs/vecty v0.0.0-20200229085432-b20ff07e496f
	github.com/r3labs/diff v0.0.0-20191120142937-b4ed99a31f5a
	github.com/stretchr/testify v1.5.1 // indirect
	gitlab.com/alienscience/parser v0.0.0-20200308135812-5bc0a9fbedb4
)
